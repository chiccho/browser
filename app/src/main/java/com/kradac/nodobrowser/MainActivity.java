package com.kradac.nodobrowser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Build;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private WebView webViewVpn;
    private ProgressDialog pDialog;
    private LayoutInflater layoutInflater;
    private View popupView;
    private AlertDialog dialog;
    public Context context = this;
    public static Activity main;
    Globals g = Globals.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        createNotificationChannel();
        if (leerDatos("servicio").equals("")) {
            g.setSSIDData("Vacio");
            g.setBReceiverData(1);
            g.setRedData(false);
        }else{
            g.setSSIDData(leerDatos("ssid"));
            g.setBReceiverData(Integer.parseInt(leerDatos("bReceiver")));
            g.setRedData(Boolean.parseBoolean(leerDatos("red")));
        }
        g.setService(true);
        main = this;
        Intent backgroundService = new Intent(context, RegisterBroadcastReceiver.class);
        startService(backgroundService);
        Intent detectServer = new Intent(context, DetectServer.class);
        startService(detectServer);

        webViewVpn=(WebView) findViewById(R.id.webViewVpn);

        WebSettings webSettings = webViewVpn.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webViewVpn.setWebViewClient(new WebViewClient());
        webViewVpn.getSettings().setBuiltInZoomControls(true);
        webViewVpn.getSettings().setDisplayZoomControls(false);
        webViewVpn.loadUrl("http://nodowifi.net");
        //mostrarProgressDiealog(getString(R.string.msg_dialog_espera));
        webViewVpn.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //    ocultarProgressDialog();
                    }
                }, 2000);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                webViewVpn.loadUrl(url.replace("https:","http:"));
                return false;
            }
            @Override
            public void onReceivedError(WebView view, int errorCod,String description, String failingUrl) {
                webViewVpn.setVisibility(View.GONE);
                //Toast.makeText(getApplicationContext(),"Error al cargar la pagina",Toast.LENGTH_LONG).show();
                g.setService(false);
                popup();
                Intent detectServer = new Intent(getApplicationContext(), DetectServer.class);
                stopService(detectServer);
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        Intent detectServer = new Intent(getApplicationContext(), DetectServer.class);
        g.setService(false);
        g.setBReceiverData(1);
        stopService(detectServer);
        try {
            FileOutputStream fos = openFileOutput("ssid", Context.MODE_PRIVATE);
            fos.write(g.getSSIDData().getBytes());
            fos.close();
            fos = openFileOutput("bReceiver", Context.MODE_PRIVATE);
            fos.write(String.valueOf(g.getBReceiverData()).getBytes());
            fos.close();
            fos = openFileOutput("red", Context.MODE_PRIVATE);
            fos.write(String.valueOf(g.getRedData()).getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Intent detectServer = new Intent(getApplicationContext(), DetectServer.class);
        g.setService(false);
        g.setBReceiverData(1);
        stopService(detectServer);
    }
/*
    @Override
    public void onResume(){
        super.onResume();
        //Intent detectServer = new Intent(getApplicationContext(), DetectServer.class);
        //startService(detectServer);
    }

    @Override
    public void onStart(){
        super.onStart();
    }
*/
    @Override
    public void onStop(){
        super.onStop();
        Intent detectServer = new Intent(getApplicationContext(), DetectServer.class);
        g.setService(false);
        g.setBReceiverData(1);
        stopService(detectServer);
    }

    public void terminar(){
        main.finish();
    }

    public void popup() {
        layoutInflater =(LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
        popupView = layoutInflater.inflate(R.layout.layout_custom_dialog, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(popupView);
        alert.setCancelable(false);
        Button btn = (Button) popupView.findViewById(R.id.btnAceptar);
        //alert.setNegativeButton("close", null);
        dialog = alert.create();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                terminar();
            }
        });
        dialog.show();
    }

    private String leerDatos(String archivo){
        String datos = "";
        try {
            FileInputStream fis = openFileInput(archivo);
            int content;
            while ((content = fis.read()) != -1) {
                datos += (char) content;
            }
            fis.close();
            return datos;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return datos;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("canalNODO", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        //IntentFilter intentFilter = new IntentFilter();
        //intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        //registerReceiver(broadcastReceiver, intentFilter);
        //Toast.makeText(getApplicationContext(), "Broadcast registrado!", Toast.LENGTH_SHORT).show();
    }

    protected void mostrarProgressDiealog(String mensaje) {
        if(pDialog!=null){
            if(pDialog.isShowing()&&!isFinishing()){
                pDialog.dismiss();
            }
        }
        if(!isFinishing()){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(mensaje);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    protected void ocultarProgressDialog() {
        if (pDialog != null&&!isFinishing()) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }

}