//
//  Copyright 2015  Google Inc. All Rights Reserved.
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

package com.kradac.nodobrowser;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

    /**
     * Getting the network info and displaying the notification is handled in a service
     * as we need to delay fetching the SSID name. If this is done when the receiver is
     * called, the name isn't yet available and you'll get null.
     *
     * As the broadcast receiver is flagged for termination as soon as onReceive() completes,
     * there's a chance that it will be killed before the handler has had time to finish. Placing
     * it in a service lets us control the lifetime.
     */

    public class WifiActiveService extends Service {
        int notificationId=1029384756;
        private Context context = this;
        //private final static String TAG = WifiActiveService.class.getSimpleName();

        @Override
        public int onStartCommand(Intent intent, int flags, final int startId) {
            //Toast.makeText(getBaseContext(), "Servicio Iniciado", Toast.LENGTH_LONG).show();
            // Need to wait a bit for the SSID to get picked up;
            // if done immediately all we'll get is null
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo info = wifiManager.getConnectionInfo();
                    String ssidRed = info.getSSID();
                    Globals s = Globals.getInstance();
                    String ssidAct = s.getSSIDData();
                    if(ssidAct == null) {
                        ssidAct = "Vacio";
                    }
                    if(!ssidRed.equals(ssidAct)){
                        if(ssidRed.equals("\"NODO WIFI GRATIS\"")){
                            s.setRedData(true);
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
                            NotificationCompat.Builder mBuilder;
                            // Set the intent that will fire when the user taps the notification
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                mBuilder = new NotificationCompat.Builder(context, "canalNODO");
                                mBuilder.setSmallIcon(R.drawable.ic_nodo_notify)
                                        .setContentTitle("NODO Wi-Fi GRATIS")
                                        .setContentText("Acceda a Internet Gratuito desde la app NODO Wi-Fi")
                                        .setStyle(new NotificationCompat.BigTextStyle()
                                                .bigText("Acceda a Internet Gratuito desde la app\nNODO Wi-Fi"))
                                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                        .setChannelId("canalNODO")
                                        .setContentIntent(pendingIntent)
                                        .setTicker("Accede a internet gratis!")
                                        .setAutoCancel(true);
                            }else{
                                mBuilder = new NotificationCompat.Builder(context, null);
                                mBuilder.setSmallIcon(R.drawable.ic_nodo_notify)
                                        .setContentTitle("NODO Wi-Fi GRATIS")
                                        .setContentText("Acceda a Internet Gratuito desde la app NODO Wi-Fi")
                                        .setStyle(new NotificationCompat.BigTextStyle()
                                                .bigText("Acceda a Internet Gratuito desde la app\nNODO Wi-Fi"))
                                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                        .setContentIntent(pendingIntent)
                                        .setTicker("Accede a internet gratis!")
                                        .setAutoCancel(true);
                            }
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(notificationId, mBuilder.build());
                        }else{
                            s.setRedData(false);
                        }
                        s.setSSIDData(ssidRed);
                    }
                    stopSelf();
                }
            }, 5000);
            return START_NOT_STICKY;
        }

        @Override
        public void onDestroy(){
            //Toast.makeText(context, "Servicio SSID Detenido", Toast.LENGTH_LONG).show();
            //Servicio detenido
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }
