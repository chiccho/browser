package com.kradac.nodobrowser;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class DetectServer extends Service {
    Context context = this;
    AlertDialog dialog;
    public DetectServer() {
    }

    @Nullable

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Globals g = Globals.getInstance();
                if(g.getService()){
                    Runtime runtime = Runtime.getRuntime();
                    try {
                        Process mIpAddrProcess = runtime.exec("/system/bin/ping -c 1 nodovideo.net");
                        int mExitValue = mIpAddrProcess.waitFor();
                        mExitValue = 0;
                        if ((mExitValue != 0)) {
                            g.setService(false);
                            MainActivity contx = (MainActivity) new MainActivity();
                            LayoutInflater layoutInflater = LayoutInflater.from(contx.main);
                            View popupView = layoutInflater.inflate(R.layout.layout_custom_dialog, null);
                            AlertDialog.Builder alert = new AlertDialog.Builder(contx.main);
                            alert.setView(popupView);
                            alert.setCancelable(false);
                            Button btn = (Button) popupView.findViewById(R.id.btnAceptar);
                            //alert.setNegativeButton("close", null);
                            btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //finish();
                                    //System.exit(0);
                                    dialog.dismiss();
                                    MainActivity noPing = new MainActivity();
                                    noPing.terminar();
                                }
                            });
                            dialog = alert.create();
                            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
                            dialog.show();
                        }
                        mIpAddrProcess.destroy();
                    } catch (InterruptedException ignore) {
                        ignore.printStackTrace();
                        System.out.println(" Exception:" + ignore);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println(" Exception:" + e);
                    }
                }
                stopSelf();
            }
        }, 10000);
        return START_NOT_STICKY;//super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Globals g = Globals.getInstance();
        if (g.getService()) {
            Intent detectServer = new Intent(context, DetectServer.class);
            startService(detectServer);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
