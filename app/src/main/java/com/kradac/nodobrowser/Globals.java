package com.kradac.nodobrowser;

public class Globals {
    private static Globals instance;

    // Global variable
    private String SSID;
    private int runned;
    private boolean service;
    private boolean red;

    // Restrict the constructor from being instantiated
    private Globals(){}

    public void setSSIDData(String d){ this.SSID =d; }
    public String getSSIDData(){
        return this.SSID;
    }

    public void setBReceiverData(int d){
        this.runned=d;
    }
    public int getBReceiverData(){
        return this.runned;
    }

    public void setService(boolean d){
        this.service=d;
    }
    public boolean getService(){
        return this.service;
    }

    public void setRedData(boolean d){
        this.red=d;
    }
    public boolean getRedData(){
        return this.red;
    }

    public static synchronized Globals getInstance(){
        if(instance==null){
            instance=new Globals();
        }
        return instance;
    }
}
