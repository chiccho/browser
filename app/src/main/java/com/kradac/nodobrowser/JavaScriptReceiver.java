package com.kradac.nodobrowser;

import android.content.Context;
import android.webkit.JavascriptInterface;

/**
 * Created by fabricio on 3/11/18.
 */

public class JavaScriptReceiver {
    private Context mContext;
    private JavaScriptListener javaScriptListener;

    public JavaScriptListener getJavaScriptListener() {
        return javaScriptListener;
    }

    public void setJavaScriptListener(JavaScriptListener javaScriptListener) {
        this.javaScriptListener = javaScriptListener;
    }

    public  interface JavaScriptListener{
        void onStartVpn(String ip);
    }

    JavaScriptReceiver(Context c) {
        mContext = c;
    }

    @JavascriptInterface
    public void startVpn(){
        String ip="192.168.90.159";
        javaScriptListener.onStartVpn(ip);
    }
}
