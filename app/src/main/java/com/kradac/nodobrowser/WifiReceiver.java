package com.kradac.nodobrowser;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class WifiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Globals s = Globals.getInstance();
        if(s.getBReceiverData()==5){
            if(!(s.getRedData())){
                context.startService(new Intent(context, WifiActiveService.class));
                s.setBReceiverData(1);
            }
        }else{
            s.setBReceiverData(s.getBReceiverData()+1);
        }
    }
}
