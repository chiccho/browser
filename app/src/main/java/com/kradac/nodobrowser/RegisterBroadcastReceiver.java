package com.kradac.nodobrowser;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class RegisterBroadcastReceiver extends Service {
    private WifiReceiver wifiReceiver = null;
    public RegisterBroadcastReceiver() {
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter intentFilter = new IntentFilter();

        // Add network connectivity change action.
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        // Set broadcast receiver priority.
        intentFilter.setPriority(100);
        // Create a network change broadcast receiver.
        wifiReceiver = new WifiReceiver();

        // Register the broadcast receiver with the intent filter object.
        registerReceiver(wifiReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister screenOnOffReceiver when destroy.
        if(wifiReceiver!=null){
            unregisterReceiver(wifiReceiver);
        }
    }
}
